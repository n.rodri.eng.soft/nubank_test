const REGULAR_TRANSFER_ID_IN = 1;
const REGULAR_TRANSFER_ID_OUT = 2;
const POSITIVE_BALANCE_FACTOR = 1;
const NEGATIVE_BALANCE_FACTOR = -1;
const TRANSFER_SYSTEM_PIX = "'Pix'";
const IN_TRANSFER = "'in'";
const OUT_TRANSFER = "'out'";
const STATUS_COMPLETED = "'completed'";


module.exports = {
    REGULAR_TRANSFER_ID_IN,
    REGULAR_TRANSFER_ID_OUT,
    TRANSFER_SYSTEM_PIX,
    POSITIVE_BALANCE_FACTOR,
    NEGATIVE_BALANCE_FACTOR,
    STATUS_COMPLETED,
    IN_TRANSFER,
    OUT_TRANSFER
}